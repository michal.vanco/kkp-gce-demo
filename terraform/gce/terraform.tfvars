cluster_name                            = "kkp-demo-gce"
region                                  = "europe-west3"
project                                 = "ps-workspace"
ssh_public_key_file                     = "~/.ssh/id_rsa.pub"
ssh_username                            = "root"
worker_os                               = "ubuntu"
workers_type                            = "n1-standard-2"
control_plane_volume_size               = 100
control_plane_target_pool_members_count = 3
control_plane_type                      = "n1-standard-2"
# More variables can be overridden here, see ../../../terraform/gce/variables.tf.
