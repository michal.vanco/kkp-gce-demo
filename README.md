# KKP on AutoPilot

This structure includes a fully automated setup of Kubermatic Kubernetes Platform
on top of gce with integration of Flux v2 for GitOps delivery and SOPS for secrets management.

## Used Components and Tools

 * Terraform - for automated resources provisioning in gce
 * KubeOne - for k8s master cluster preparation
 * KKP installer - for installing core KKP components on master cluster
 * Flux v2 - for managing k8s resources on master cluster with GitOps
 * SOPS, using Age backend - for safe storage of sensitive setup in GitHub
 * GitLab CI/CD - for the fully fledged delivery pipeline, 0 steps are done manually
   (except the `git init`, `git commit` and `git push`!)

## Preparation
### Create GitLab repository

Create new repository on GitLab [manually](https://docs.gitlab.com/ee/user/project/working_with_projects.html#create-a-project).

Also prepare a GitLab API token which will be used for GitOps setup.

### Get your gce credentials
You need a Service Account with the appropriate permissions for Terraform to create the infrastructure and for machine-controller to create worker nodes.

Use `gcloud` CLI tool to create these credentials.

```bash
# create new service account
gcloud iam service-accounts create k1-cluster-provisioner
# get your service account id
gcloud iam service-accounts list
# get your project id
gcloud projects list
# create policy binding
gcloud projects add-iam-policy-binding YOUR_PROJECT_ID --member 'serviceAccount:YOUR_SERVICE_ACCOUNT_ID' --role='roles/compute.admin'
gcloud projects add-iam-policy-binding YOUR_PROJECT_ID --member 'serviceAccount:YOUR_SERVICE_ACCOUNT_ID' --role='roles/iam.serviceAccountUser'
gcloud projects add-iam-policy-binding YOUR_PROJECT_ID --member 'serviceAccount:YOUR_SERVICE_ACCOUNT_ID' --role='roles/viewer'
gcloud projects add-iam-policy-binding YOUR_PROJECT_ID --member 'serviceAccount:YOUR_SERVICE_ACCOUNT_ID' --role='roles/storage.admin'
# create a new json key for your service account
gcloud iam service-accounts keys create --iam-account YOUR_SERVICE_ACCOUNT_ID k1-cluster-provisioner-sa-key.json
# export JSON file content of created service account json key
export GOOGLE_CREDENTIALS=$(cat ./k1-cluster-provisioner-sa-key.json)
```

See [KubeOne documentation](https://docs.kubermatic.com/kubeone/master/architecture/requirements/machine_controller/google_cloud/gcp/) for more details.

### Generate SSH keys

SSH public/private key-pair is used for accessing the master cluster nodes. You can generate these keys locally,
and you will need to set them inside the secret management below.

You can use following command to generate the keys:

```bash
ssh-keygen -t rsa -b 4096 -C "admin@kubermatic.com"
```

You will be prompted to provide a key location, e.g. `~/.ssh/k8s_rsa`, if you choose different location
make sure to update the `ssh_public_key_file` variable in terraform!
### Setup GitLab CI/CD Variables for the pipeline

Go to your GitLab project under "Settings" -> "CI/CD" -> "Variables" and setup following variables:
 * `SOPS_AGE_SECRET_KEY` with value of generated AGE secret key for SOPS (see secrets.md file)
 * `GITLAB_TOKEN` with value of GitLab API token from above step
 * `SSH_PRIVATE_KEY` with value of private SSH key (e.g. content of `~/.ssh/k8s_rsa`), make sure to create variable with "Type" `Variable`
 * `SSH_PUBLIC_KEY` with value of public SSH key (e.g. content of `~/.ssh/k8s_rsa.pub`), make sure to create variable with "Type" `File`

### Push Content to your Git repository

```bash
git init
git checkout -b main
git add .
git commit -m "Initial setup for KKP on Autopilot"
git remote add origin git@gitlab.com:<GITLAB_OWNER>/<GITLAB_REPOSITORY>
git push -u origin main
```

### Validation
Check the steps of the GitLab CI/CD Pipeline after first merge to `main` branch and enjoy the full deployment of KKP at the end!

## High-level Pipeline Design

*tf-validate*
* runs validation of all Terraform modules

*tf-prepare*
* prepare Terraform backend for storing state

*tf-plan*
* prepare Terraform plan based on the stored state
* Terraform state is stored on AWS S3 bucket (created in previous step)

*tf-apply*
* applies the Terraform changes based on a plan (from previous stage)
* runs only on `main` branch
==> VMs, network and LB for k8s on AWS is prepared at this stage

*kubeone-apply*
* performs the cluster provisioning using the `kubeone` tool
* runs only on `main` branch!
==> k8s cluster is ready to use at this stage

*kkp-deploy*
* performs the Kubermatic Kubernetes Platform installation with installer
* runs only on `main` branch!
==> KKP platform with core components is prepared at this stage

*flux-bootstrap*
* initiate Flux v2 using `flux bootstrap gitlab` command
* runs only on `main` branch!
==> monitoring stack for KKP and other KKP resources (seed, preset, project) are delivered after Flux is set up on cluster,
Flux itself is also managed by the same GitHub repository

## Secrets management

Sensitive values are encrypted using the [SOPS](https://fluxcd.io/docs/guides/mozilla-sops/)
together with using the Age secret pair (generated from start.kubermatio.io).

Public AGE key for encryption is: `age1as7e5eqsj8jcmnqtx9jsgn7wmutykvw9ftf9xc5l34z26zctdszqfuuxmm`.

## Operational tasks

See the various operational tasks in documentation:
 * [get access to Kubernetes cluster](https://docs.kubermatic.com/kubermatic/master/startio/cheat_sheets/access_to_cluster/)
 * [validate cluster and KKP readiness](https://docs.kubermatic.com/kubermatic/master/startio/cheat_sheets/validate_cluster_health/)
 * [work with secrets using SOPS](https://docs.kubermatic.com/kubermatic/master/startio/cheat_sheets/work_with_secrets/)
 * and [others](https://docs.kubermatic.com/kubermatic/master/startio/cheat_sheets/).
